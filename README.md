# Makemkv Automation Utils

This packages provides a series of utilities for handling mkv like:

-   Quickly ripping movies and series with [The Movie DB](https://www.themoviedb.org) info and organizing them to import in jellyfin.
-   Search for mkv files and select optimal audio and subtitles tracks.

## Table of contents

-   [Makemkv Automation Utils](#makemkv-automation-utils)
    -   [Table of contents](#table-of-contents)
    -   [Getting Started](#getting-started)
        -   [Prerequisites](#prerequisites)
        -   [Installation](#installation)
    -   [Usage](#usage)
        -   [Running the app](#running-the-app)
    -   [Contributing](#contributing)
    -   [Credits](#credits)
    -   [Built With](#built-with)
    -   [Versioning](#versioning)
    -   [Authors](#authors)
    -   [License](#license)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This project requires NodeJS (version 10 or later) and NPM.
[Node](http://nodejs.org/) and [NPM](https://npmjs.org/) are really easy to install.
To make sure you have them available on your machine,
try running the following command.

```sh
$ npm -v && node -v
6.14.8
v14.15.0
```

Depending on the utilities used [Makemkv](https://www.makemkv.com/) or [MKVToolNix](https://mkvtoolnix.download/downloads.html) (or both) command line programs will be neccesary. Follow steps provided in each site to install them.
Verify thier availability with the following command.

```sh
$ makemkvcon ; mkvmerge -V ; mkvpropedit -V
Use: makemkvcon [switches] Command [Parameters]
...
mkvmerge v51.0.0 ('I Wish') 64-bit
mkvpropedit v51.0.0 ('I Wish') 64-bit
```

### Installation

For using the tool install it with npm

```sh
npm i -g makemkv-utils
```

## Usage

### Running the app

```sh
$ mkvutils
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

1.  Fork it!
2.  Create your feature branch: `git checkout -b my-new-feature`
3.  Add your changes: `git add .`
4.  Commit your changes: `git commit -am 'Add some feature'`
5.  Push to the branch: `git push origin my-new-feature`
6.  Submit a pull request :sunglasses:

## Built With

-   [Yarn](https://yarnpkg.com/)
-   [Visual Studio Code](https://code.visualstudio.com/)
-   [Postman](https://www.postman.com/)
-   [Jellyfin](https://jellyfin.org/)
-   [Makemkv](https://www.makemkv.com/)
-   [MKVToolNix](https://mkvtoolnix.download/downloads.html)
-   ![The Movie DB](https://www.themoviedb.org/assets/2/v4/logos/v2/blue_long_2-9665a76b1ae401a510ec1e0ca40ddcb3b0cfe45f1d51b77a308fea0845885648.svg) [The Movie DB API](https://www.themoviedb.org/documentation/api)
    > This product uses the TMDb API but is not endorsed or certified by TMDb
-   [moviedb-promise](https://www.npmjs.com/package/moviedb-promise)
-   [axios](https://www.npmjs.com/package/axios)
-   [inquirer](https://www.npmjs.com/package/inquirer)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/davidgoitia/maekmkv-utils/tags).

## Authors

-   **David Goitia** - _Initial work_ - [GitLab](https://gitlab.com/davidgoitia) [GitHub](https://gitlab.com/davidgoitia)

See also the list of [contributors](https://gitlab.com/davidgoitia/makemkv-utils/contributors) who participated in this project.

## License

[MIT License](https://davidgoitia.mit-license.org/2020) © David Goitia
