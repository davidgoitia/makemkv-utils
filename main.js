#!/usr/bin/env node

/* eslint-disable max-len */
const makemkv = require('./src/makemkv');
const mkvtn = require('./src/mkvToolNix');
const diskutil = require('./src/diskutil');
const cons = require('./src/console');
const tmbd = require('./src/tmbd');
const log = require('./src/log');
const chalk = require('chalk');
const path = require('path');
const fs = require('fs');
const os = require('os');
require('dotenv').config();

let disc = parseFloat(process.env.disc);
let baseDir = process.env.baseDir;

/**
 * Resolves ~ in a path
 * @param {String} file path to be resolved
 * @return {String} resolved path
 */
function resolveHome(file) {
  if (file[0] === '~') {
    if (file.length === 1) return os.homedir();
    const bar = file.indexOf('/');
    if (bar < 0) return path.resolve(os.homedir(), `../${file.slice(1)}`);
    return path.join(
            bar > 1 ?
                path.resolve(os.homedir(), `../${file.substring(1, bar)}`) :
                os.homedir(),
            file.slice(bar + 1),
    );
  }
  return file;
}

/**
 * Selects a new baseDir if not already settled or specified
 * @param {Boolean} ask true if should be asked even if value already setted
 * @return {Promise} promise fulfilled when selection terminated
 */
async function selectBaseDir(ask = true) {
  if (ask || !baseDir) {
    baseDir = await cons.simpleQuery('Base dir', {
      default: baseDir || process.cwd(),
      filter: resolveHome,
      validate: (dir) =>
        fs.promises
            .access(dir, fs.constants.W_OK | fs.constants.F_OK)
            .then(() => fs.promises.stat(dir))
            .then((s) =>
                        s.isDirectory() ?
                            true :
                            chalk.red.bold(`"${dir}" isn't a directory`),
            )
            .catch(log.logError),
    });
  } else {
    console.log(chalk.green.bold('Base dir:'), chalk.cyan(baseDir));
  }
}

/**
 * Selects a new disc ID if not already settled or specified
 * @param {Boolean} ask true if should be asked even if value already setted
 * @return {Promise} promise fulfilled when selection terminated
 */
async function selectDisc(ask = true) {
  if (ask || isNaN(disc)) {
    disc = await cons.simpleQuery(
        'Integer disc ID to use',
        'query and select',
    );
  } else {
    console.log(chalk.green.bold('Disc ID:'), chalk.cyan(disc));
  }
  if (isNaN(disc)) disc = await cons.selectDisc(await makemkv.listDrives());
}

/**
 * Config menu
 * @return {Promise} promise fulfilled when menu an ops terminated
 */
function config() {
  return cons.menu(
      'Config options',
      {
        'Select disc': selectDisc,
        'Set language': () =>
          cons
              .simpleQuery(
                  'TMBd results language like \'es-ES\' or \'en-US\'',
                  tmbd.getLang(),
              )
              .then(tmbd.setLang),
        'Change base dir': selectBaseDir,
        'Change TMBd API key': cons.askApiKey,
      },
      'Back',
  );
}

/**
 * Manual ops menu
 * @return {Promise} promise fulfilled when menu an ops terminated
 */
function manualOps() {
  return cons.menu(
      'Manual operations',
      {
        'Eject': () => diskutil.eject(disc),
        'Close': () => diskutil.close(disc),
        'Scan': async () =>
          console.log(
              chalk.blue.bold('Disc info: '),
              await makemkv.scanDisk(true, disc),
          ),
        'Change MKV language': async () => {
          const file = await cons.simpleQuery('File path');
          console.log(
              chalk.green.bold('Result'),
              await mkvtn.select(
                  file,
                  await mkvtn.info(file),
                  await cons.selectTracks(file),
              ),
          );
        },
      },
      'Back',
  );
}

/**
 * Writes the info retrieved of a title to disk for making it available for future use
 * @param {String} dir path for base dir
 * @param {String} title name of the movie or series dir
 * @param {MakeMKVInfo} info object containing data of the title
 * @param {Boolean} rip true if rip process, false for info
 */
async function writeDiscInfo(dir, title, info, rip = false) {
  dir = path.join(dir, title);
  await makemkv.mkdir(dir);
  return fs.promises.writeFile(
      path.join(dir, `.${rip ? 'riplog' : 'mkvinfo'}.${info.rawName}.json`),
      JSON.stringify(info, null, 4),
  );
}

/**
 * Movies process
 * @return {Promise} promise fulfilled when selections terminated
 */
async function movies() {
  do {
    diskutil.eject(disc);
    const movie = await cons.queryMedia(true);
    console.log(
        chalk.green.bold('waiting insetion of disc'),
        chalk.magenta.bold(movie.jellyfin.full),
    );
    const titles = await makemkv.scanDisk(true, disc);
    await writeDiscInfo(baseDir, movie.jellyfin.full, titles);
    // console.log(chalk.blue.bold('Scan result: '), titles);
    const rip = await makemkv.rip(
        path.join(baseDir, movie.jellyfin.full),
        disc,
    );
    // console.log( chalk.blue.bold('Rip result: '), rip);
    rip.messages
        .filter((m) => m.code === 5003)
        .forEach((m) => console.log(chalk.red.bold(m.message)));
    writeDiscInfo(
        baseDir,
        movie.jellyfin.full,
        Object.assign(rip, {rawName: titles.rawName}),
        true,
    );
    await diskutil.eject(disc);
    await makemkv.renameFiles(baseDir, titles, movie);
  } while (await cons.goOn('Continue with movies'));
}

/**
 * Movies process
 * @return {Promise} promise fulfilled when selections terminated
 */
async function series() {
  const serie = await cons.queryMedia(false);
  const extras = {extras: 1};
  do {
    diskutil.eject(disc);
    console.log(
        chalk.green.bold('waiting insetion of disc'),
        chalk.magenta.bold(serie.jellyfin.full),
        chalk.green.bold('season'),
        chalk.magenta.bold(serie.jellyfin.season),
        chalk.green.bold(' episode'),
        chalk.magenta.bold(serie.jellyfin.episode),
        chalk.blue.bold(tmbd.getSeason(serie, true).jellyfin.title),
    );
    const titles = await makemkv.scanDisk(true, disc);
    await writeDiscInfo(baseDir, serie.jellyfin.full, titles);
    // console.log(chalk.blue.bold('Scan result: '), titles);
    const rip = await makemkv.rip(
        path.join(baseDir, serie.jellyfin.full),
        disc,
    );
    // console.log( chalk.blue.bold('Rip result: '), rip);
    rip.messages
        .filter((m) => m.code === 5003)
        .forEach((m) => console.log(chalk.red.bold(m.message)));
    writeDiscInfo(
        baseDir,
        serie.jellyfin.full,
        Object.assign(rip, {rawName: titles.rawName}),
        true,
    );
    await diskutil.eject(disc);
    await makemkv.renameFiles(baseDir, titles, serie, extras);
  } while (
    await cons.goOn(
        [
          'Continue with series',
          chalk.magenta.bold(serie.jellyfin.full),
          chalk.green.bold('season'),
          chalk.magenta.bold(serie.jellyfin.season),
          chalk.green.bold(' episode'),
          chalk.magenta.bold(serie.jellyfin.episode),
          chalk.blue.bold(tmbd.getSeason(serie, true)?.jellyfin?.title),
        ].join(' '),
    )
  );
}

/**
 * Main menu
 * @return {Promise} promise fulfilled when menu an ops terminated
 */
function main() {
  return cons.menu(
      'MakeMKV utilities',
      {
        'Movies': movies,
        'Series': series,
        'Auto MKV lang': async () => {
          const dir = await cons.simpleQuery(
              'Base dir to search mkv',
              baseDir,
          );
          const language = await cons.simpleQuery(
              'Language to configure',
              process.env.mkvLang || 'spa',
          );
          const files = await mkvtn.findMkv(dir);
          console.log(
              chalk.green.bold('Results'),
              await Promise.allSettled(
                  files.map((file) => mkvtn.selectAuto(file, language)),
              ),
          );
        },
        'Manual ops': manualOps,
        // 'Rename titles': renameTitles,
        'Config': config,
      },
      'Quit',
  );
}

/**
 * Initial settings
 * @return {Promise} promise fulfilled when menu an ops terminated
 */
async function init() {
  await selectBaseDir(false);
  await selectDisc(false);
}

init().then(main).catch(log.logError);
