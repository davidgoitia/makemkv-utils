const util = require('util');
const exec = require('child_process').exec;
const execPromise = util.promisify(exec);
const path = require('path');
const glob = require('glob');
const globPromise = util.promisify(glob);

/**
 * Searches mkv files in specified dir
 * @param {String} baseDir path for dir to find mkv
 * @return {Promise<String[]>} path for mkv files
 */
function findMkv(baseDir) {
  return globPromise(path.resolve(baseDir, '**/*.mkv'));
}

/**
 * Analizes tracks and info from MKV file
 * @param {String} file path for file to analize
 * @return {Promise<Info>} info of MKV file
 */
function info(file) {
  return execPromise(`mkvmerge -J '${file}'`).then((o) =>
    JSON.parse(o.stdout),
  );
}

/**
 * @param {Info} info info of MKV file
 * @param {String|Number} query uid or type 'audio', 'video', 'subtitles'
 * @return {Track[]|Track} track/s for type or uid
 */
function getTracks(info, query) {
  const num = parseInt(query);
  return isNaN(num) ?
        info.tracks.filter((t) => t.type === query) :
        info.tracks.find((t) => t.properties?.uid === num);
}

/**
 * Automatically tries to create selecion for desired language
 * @param {Info} info info of MKV file
 * @param {String} language preferred language in ISO-639-2B
 * @return {{audio:Number, subtitles:Number}} newDefaults for select
 */
function autoLang(info, language = 'spa') {
  const result = {
    audio: getTracks(info, 'audio')
        .filter((t) => t.properties?.language === language)
        .sort(
            (f, s) =>
              s.properties.audio_channels - f.properties.audio_channels,
        )
        .shift()?.properties?.uid,
    subtitles: undefined,
  };
  if (result.audio === undefined) {
    delete result.audio;
    delete result.subtitles;
    let defSub = getTracks(info, 'subtitles').find(
        (t) => t.properties?.default_track,
    );
    defSub =
            defSub?.properties?.language === language ?
                defSub :
                getTracks(info, 'subtitles').find(
                    (t) => t.properties?.language === language,
                );
    if (defSub) result.subtitles = defSub.properties.uid;
  }
  return result;
}

/**
 * @param {Info} info info of MKV file
 * @return {{audio:Number, subtitles:Number}} default tracks
 */
function getDefaults(info) {
  const result = {
    audio: getTracks(info, 'audio').find((t) => t.properties?.default_track)
        ?.properties?.uid,
    subtitles: getTracks(info, 'subtitles').find(
        (t) => t.properties?.default_track,
    )?.properties?.uid,
  };
  if (result.audio === undefined) delete result.audio;
  if (result.subtitles === undefined) delete result.subtitles;
  return result;
}

/**
 * Analizes tracks and info from MKV file and selects the desired language
 * @param {String} file path for file to analize
 * @param {String} language lang to select on MKV
 * @return {Promise} promise fulfilled when process is completed
 */
async function selectAuto(file, language) {
  const mkv = await info(file);
  return select(file, mkv, await autoLang(mkv, language));
}

/**
 * Analizes tracks and info from MKV file
 * @param {String} file path for file to analize
 * @param {Info} info data from mkvmerge
 * @param {{audio:Number, subtitles:Number}} newDefaults new tracks
 * @return {Promise} promise fulfilled when process is completed
 */
async function select(file, info, newDefaults = {}) {
  const data = [];
  const actual = getDefaults(info);
  if (newDefaults.audio !== actual.audio) {
    if (actual.audio !== undefined) data.push({t: actual.audio, v: 0});
    if (newDefaults.audio !== undefined) {
      data.push({t: newDefaults.audio, v: 1});
    }
  }
  if (newDefaults.subtitles !== actual.subtitles) {
    if (actual.subtitles !== undefined) {
      data.push({t: actual.subtitles, v: 0});
    }
    if (newDefaults.subtitles !== undefined) {
      data.push({t: newDefaults.subtitles, v: 1});
    }
  }
  if (data.length) {
    const cmd = `mkvpropedit '${file}' ${data
        .map((d) => `--edit track:${d.t} --set flag-default=${d.v}`)
        .join(' ')}`;
    console.debug('issuing cmd:', cmd);
    return execPromise(cmd).then((o) => o.stdout);
  }
  console.debug('No command to issue');
}

module.exports = {
  findMkv,
  info,
  getDefaults,
  autoLang,
  getTracks,
  select,
  selectAuto,
};
