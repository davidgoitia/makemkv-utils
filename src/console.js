const chalk = require('chalk');
const langs = require('langs');
const inquirer = require('inquirer');
const tmbd = require('./tmbd');
const log = require('./log');
const makemkv = require('./makemkv');
const mkvtn = require('./mkvToolNix');

/**
 * Asks onbd API key if necessary or skip is false
 * @param {Boolean} skip true if question should be skipped if value present
 */
async function askApiKey(skip = false) {
  if (skip && tmbd.getKey()) return tmbd.getKey();
  const answers = await inquirer.prompt([
    {
      type: 'input',
      name: 'key',
      default: tmbd.getKey(),
      message: 'ombd API key:',
    },
  ]);
  tmbd.setKey(answers.key);
  return answers.key;
}

/**
 * Queries the user for a single string
 * @param {String} message text to prompt the user
 * @param {String|Object} def text to prompt the user or inquirer options
 * @return {Promise} the text introduced by the user
 */
function simpleQuery(message, def) {
  if (typeof def !== 'object') def = {default: def};
  return inquirer
      .prompt([
        {
          type: 'input',
          name: 'data',
          ...def,
          message: chalk.bold.green(`${message}:`),
        },
      ])
      .then((a) => a.data);
}

/**
 * Presents a series of optiosn to the client
 * @param {String} title title of the menu
 * @param {Object|Array} options array of texts or object mapping the text
 *  and functions to call
 * @param {String} quit text to show if menu should be repeated
 *  until quit option selected
 * @return {Promise} returns a promise that is fulfilled when menu
 *  and option selected are exited
 */
async function menu(title, options, quit) {
  let selected;
  do {
    selected = await inquirer
        .prompt({
          type: 'list',
          name: 'next',
          message: title,
          choices: Array.isArray(options) ?
                    quit ?
                        options.push(quit) :
                        options :
                    Object.keys(
                          quit ? {...options, [quit]: undefined} : options,
                    ),
        })
        .then((a) => a.next);
    if (!Array.isArray(options) && options[selected]) {
      await options[selected]();
    }
  } while (quit && selected !== quit);
  return selected;
}

/**
 * Asks the user for continuation
 * @param {String} text question to promp the user
 * @param {Boolean} def default value. True
 * @return {Promise<Boolean>} promise with selected answer
 */
function goOn(text = 'Continue', def = true) {
  return inquirer
      .prompt({
        type: 'confirm',
        name: 'yes',
        message: `${text}?`,
        default: def,
      })
      .then((a) => a.yes)
      .catch((err) => {
        log.logError(err);
        return false;
      });
}

/**
 * Asks the user for name of series or movie
 * @param {Boolean} movie true if movie. false if series
 */
async function queryMedia(movie = true) {
  let media = {jellyfin: {}};
  let sure = false;
  do {
    media.jellyfin.full = await simpleQuery(
        `${movie ? 'Movie' : 'Serie'} name`,
        media.jellyfin.full,
    );
    try {
      if (askApiKey(true)) {
        const titles = await tmbd.search(media.jellyfin.full, movie);
        if (!titles.length) {
          console.warn(chalk.bold.redBright('No media found'));
          continue;
        }
        media = await menu(
            `Select ${movie ? 'movie' : 'serie'}`,
            titles.map((t) => ({
              name:
                            chalk.magenta(t.jellyfin.year, '\t') +
                            chalk.green(t.popularity, '\t') +
                            chalk.bold.blue(t.jellyfin.title),
              value: t,
              short:
                            chalk.magenta(t.jellyfin.year, '\t') +
                            chalk.bold.blue(t.jellyfin.title),
            })),
        );
        media.jellyfin.full = await simpleQuery(
            'Edit title',
            media.jellyfin.full,
        );
        sure = Boolean(media.jellyfin.full);
      } else {
        sure = await goOn(
            chalk.magenta.bold(media.jellyfin.full) + ', are you sure',
        );
      }
    } catch (err) {
      log.logError(err);
    }
  } while (!sure);
  if (!movie) {
    if (askApiKey(true)) {
      media = Object.assign(await tmbd.seriesInfo(media.id), {
        jellyfin: media.jellyfin,
      });
      media.jellyfin.season = await menu(
          `Select starting season`,
          media.seasons.map((s) => ({
            name:
                        chalk.magenta(s.season_number, '\t') +
                        chalk.bold.blue(s.jellyfin.title),
            value: s.season_number,
          })),
      );
      media.jellyfin.episode = await menu(
          `Select starting episode`,
          tmbd.getSeason(media).episodes.map((s) => ({
            name:
                        chalk.magenta(s.episode_number, '\t') +
                        chalk.bold.blue(s.jellyfin.title),
            value: s.episode_number,
          })),
      );
    } else {
      media.jellyfin.season = parseInt(await simpleQuery('Season', 1));
      media.jellyfin.episode = parseInt(await simpleQuery('Episode', 1));
    }
    // eslint-disable-next-line max-len
    media.jellyfin.min = await simpleQuery(
        'Minimum episode length HH:MM:SS, emtpy for auto',
    );
    if (media.jellyfin.min) {
      media.jellyfin.min = makemkv.durationToSecs(media.jellyfin.min);
    }
    media.jellyfin.max = await simpleQuery(
        'Maximum episode length HH:MM:SS, emtpy for auto',
    );
    if (media.jellyfin.max) {
      media.jellyfin.max = makemkv.durationToSecs(media.jellyfin.max);
    }
  }
  return media;
}

/**
 * Queries the user for a single string
 * @param {MakeMkvInfo} mkvInfo text to prompt the user
 * @return {Promise<Number>} returns the id of the selected disc
 */
function selectDisc(mkvInfo) {
  if (!mkvInfo?.drives?.length) throw new Error('No valid drives detected');
  return menu(
      'Select disk unit',
      mkvInfo.drives.map((disc) => ({
        name:
                chalk.magenta(disc.id, '\t') +
                chalk.bold.blue(disc.drive, '\t') +
                chalk.green(disc.device),
        value: disc.id,
        short: chalk.blue.bold(disc.drive),
      })),
  );
}

const prettyAudio = (t) =>
  chalk.magenta(t.properties.uid, '\t') +
    chalk.bold.blue(langs.where('2B', t.properties.language).name, '\t') +
    chalk.green(t.properties.track_name);

const prettySubtitles = (t) =>
  chalk.magenta(t.properties.uid, '\t') +
    chalk.bold.blue(langs.where('2B', t.properties.language).name);

/**
 * Queries the user for a single string
 * @param {String} file path to file to select
 * @return {{audio:Number, subtitles:Number}} newDefaults new tracks
 */
async function selectTracks(file) {
  const mkv = await mkvtn.info(file);
  const actual = mkvtn.getDefaults(mkv);
  const result = {};
  const audio = mkvtn.getTracks(mkv, 'audio');
  if (audio.length) {
    result.audio = await menu(
        `Select audio track. Actual ${
                actual.audio ?
                    prettyAudio(mkvtn.getTracks(mkv, actual.audio)) :
                    chalk.red.bold('None')
        }`,
        audio.map((t) => ({
          name: prettyAudio(t),
          value: t.properties.uid,
        })),
    );
  } else {
    console.log(chalk.yellow.bold('No audio found'));
  }
  const subtitles = mkvtn.getTracks(mkv, 'subtitles');
  if (subtitles.length) {
    result.subtitles = await menu(
        `Select subtitles track. Actual ${
                actual.subtitles ?
                    prettySubtitles(mkvtn.getTracks(mkv, actual.subtitles)) :
                    chalk.red.bold('None')
        }`,
        [
          ...subtitles.map((t) => ({
            name: prettySubtitles(t),
            value: t.properties.uid,
          })),
          {
            name: chalk.red('Remove'),
            value: undefined,
          },
        ],
    );
  } else {
    console.log(chalk.yellow.bold('No subtitles found'));
  }
  return result;
}

module.exports = {
  askApiKey,
  simpleQuery,
  selectDisc,
  menu,
  goOn,
  queryMedia,
  selectTracks,
};
