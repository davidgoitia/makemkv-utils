const util = require('util');
const exec = util.promisify(require('child_process').exec);
const makemkv = require('./makemkv');

/**
 * (Linux) Gets device info from drive ID
 * @param {String|Number} id numeric id of the drive
 * @return {Promise<String>} path of the device
 */
function getDeviceForID(id) {
  return id && !isNaN(parseInt(id)) ?
        makemkv.listDrives(false, true).then((info) => info.drives[id].device) :
        Promise.resolve(id || '');
}

/**
 * Eject the specified disk drive.
 * @param {int|string} id Locator for the disk drive.
 */
async function eject(id) {
  if (process.platform === 'darwin') {
    await exec('drutil tray eject ' + id || 1);
    // are we running on linux?
  } else if (process.platform === 'linux') {
    await exec('eject ' + (await getDeviceForID(id)));
  } else {
    throw new Error('unsupported platform: ' + process.platform);
  }
}

/**
 * Close the specified disk drive.
 * @param {int|string} id Locator for the disk drive.
 */
async function close(id) {
  if (process.platform === 'darwin') {
    await exec('drutil tray close ' + id || 1);
    // are we running on linux?
  } else if (process.platform === 'linux') {
    await exec('eject -t ' + (await getDeviceForID(id)));
  } else {
    throw new Error('unsupported platform: ' + process.platform);
  }
}

module.exports = {
  eject,
  close,
};
