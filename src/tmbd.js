const removeAccents = require('remove-accents');
const {MovieDb} = require('moviedb-promise');
require('dotenv').config();

const moviedb = new MovieDb(
    process.env.apiKey || 'a0cf0071584f9b58f40321bca31aead9',
);
let language;
setLang(process.env.language || process.env.LANG);

/**
 * @param {String} lang Language for translatable fields
 */
function setLang(lang) {
  language = lang?.split('.', 1).shift().replace(/_/g, '-');
}

/**
 * @return {String} Language for translatable fields
 */
function getLang() {
  return language;
}

/**
 * @param {String} key TMBD api v3 key to use
 */
function setKey(key) {
  moviedb.apiKey = key;
}

/**
 * @return {String} TMBD api v3 key being used
 */
function getKey() {
  return moviedb.apiKey;
}

/**
 * Gets info of a series
 * @param {number} serieID id of the serie
 * @return {Promise} results with list of seasons
 */
async function seriesInfo(serieID) {
  const query = {id: serieID, language};
  let series = await moviedb.tvInfo(query);
  query.append_to_response = series.seasons
      .map((s) => `season/${s.season_number}`)
      .join(',');
  // console.log('query', query);
  series = await moviedb.tvInfo(query);
  series.seasons.forEach((s) => {
    const index = `season/${s.season_number}`;
    series[index].episodes.forEach(jellyfinMap);
    Object.assign(s, jellyfinMap(series[index]));
    series[index] = undefined;
    delete series[index];
  });
  return jellyfinMap(series);
}

/**
 * Formats jellyfin usable data
 * @param {Object} t object from query representing movie, series, episodes...
 * @return {t} object with added data
 */
function jellyfinMap(t) {
  const jellyfin = {};
  jellyfin.year = (
    t?.first_air_date ||
        t?.release_date ||
        t?.air_date
  )?.substr(0, 4);
  jellyfin.title = t.title || t.name || t.original_name;
  jellyfin.safe = removeAccents(jellyfin.title).replace(/[^\w ]+/g, '');
  jellyfin.full = `${jellyfin.safe} (${jellyfin.year})`;
  return Object.assign(t, {jellyfin});
}

/**
 * @param {Number} season ID of the season to find
 * @return {Function} filter to find desired season
 */
function findSeason(season) {
  return (s) => s.season_number === season;
}

/**
 * @param {Number} episode ID of the season to find
 * @return {Function} filter to find desired season
 */
function findEpisode(episode) {
  return (s) => s.episode_number === episode;
}

/**
 * Extracts info of seasons or episodes
 * @param {Info} serie info of series
 * @param {Boolean} episode true if want to extrar episode info
 * @return {Season|Episode} season or episode found
 */
function getSeason(serie, episode = false) {
  const season = serie?.seasons?.find(findSeason(serie.jellyfin.season));
  return episode ?
        season?.episodes?.find(findEpisode(serie.jellyfin.episode)) :
        season;
}

/**
 * Queries the API for a list of shows or movies
 * @param {String} name name or part of film or show
 * @param {Boolean} movie true if movies, false if series
 * @return {Promise<Title[]>} results found
 */
async function search(name, movie = true) {
  const query = {
    query: name,
    language,
    page: 1,
  };
  const year = name.match(/(.+)\s*\((.*)\)/);
  if (year) {
    query.query = year[1];
    query.year = year[2];
  }
  const fun = movie ? moviedb.searchMovie : moviedb.searchTv;
  const results = [];
  let result;
  do {
    result = await fun.apply(moviedb, [query]);
    results.push(...result.results.map(jellyfinMap));
  } while (query.page++ < result.total_pages);
  return results.sort((f, s) => s.popularity - f.popularity);
}

module.exports = {
  moviedb,
  getLang,
  setLang,
  setKey,
  getKey,
  search,
  seriesInfo,
  findSeason,
  findEpisode,
  getSeason,
};
