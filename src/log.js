const chalk = require('chalk');

exports.logError = (err) =>
  console.error(
      Object.assign(err, {
        message: chalk.red.bold(err.message),
      }),
  );
