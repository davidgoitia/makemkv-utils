// https://www.makemkv.com/developers/usage.txt

const readline = require('readline');
const util = require('util');
const exec = require('child_process').exec;
const execPromise = util.promisify(exec);
const cliProgress = require('cli-progress');
const fsp = require('fs/promises');
const path = require('path');
const chalk = require('chalk');
const tmbd = require('./tmbd');
const log = require('./log');

/**
 * Parses a line of MakeMKV output
 * @param {String} line text to process
 * @return {MakeMKVLine} parsed info
 */
function parseMsgLine(line = '') {
  const result = {};
  let args
    ;[result.type, ...args] = line.split(':');
  args = args
      .join(':')
      .match(/[^",]+|"(\\"|[^"])*"/g)
      .map((s) => s.replace(/^"|"$/g, '').replace(/\\"/g, '"'));
  switch (result.type) {
    case 'MSG': // Message output
      ;[
        result.code,
        result.flags,
        result.count,
        result.message,
        result.format,
        ...result.params
      ] = args;
      result.code = parseFloat(result.code);
      result.flags = parseFloat(result.flags);
      result.count = parseFloat(result.count);
      break;
    case 'PRGC': // Current and total progress title
    case 'PRGT':
      ;[result.code, result.id, result.text] = args;
      result.code = parseFloat(result.code);
      result.id = parseFloat(result.id);
      break;
    case 'PRGV': // Progress bar values for current and total progress
      ;[result.current, result.total, result.max] = args;
      result.current = parseFloat(result.current);
      result.total = parseFloat(result.total);
      result.max = parseFloat(result.max);
      break;
    case 'DRV': // Drive scan messages
      ;[
        result.id,
        result.visible,
        result.enabled,
        result.flags,
        result.drive,
        result.disc,
        result.device,
      ] = args;
      result.id = parseFloat(result.id);
      result.visible = parseFloat(result.visible);
      result.enabled = parseFloat(result.enabled);
      result.flags = parseFloat(result.flags);
      break;
    case 'CINFO': // Disc information
      ;[result.code, result.subcode, result.value] = args;
      result.code = parseFloat(result.code);
      result.subcode = parseFloat(result.subcode);
      break;
    case 'TINFO': // Title information
      ;[result.id, result.code, result.subcode, result.value] = args;
      result.id = parseFloat(result.id);
      result.code = parseFloat(result.code);
      result.subcode = parseFloat(result.subcode);
      break;
    case 'SINFO': // Stream information
      ;[
        result.tid,
        result.id,
        result.code,
        result.subcode,
        result.value,
      ] = args;
      result.tid = parseFloat(result.tid);
      result.id = parseFloat(result.id);
      result.code = parseFloat(result.code);
      result.subcode = parseFloat(result.subcode);
      break;
    case 'TCOUNT': // Titles count
      ;[result.value] = args;
      result.value = parseFloat(result.value);
      break;
  }
  return result;
}

/**
 * Converts duration string to seconds
 * @param {String} duration duration expressed in "HH:MM:SS"
 * @return {Number} duration in seconds
 */
function durationToSecs(duration) {
  const [hours, mins, secs] = duration.split(':').map(parseFloat);
  return hours * 3600 + mins * 60 + secs;
}

/**
 * Adds blocks of text or processed info for fullfilling global info
 * @param {Object} info previous object for adding messages
 * @param  {...String|MakeMKVLine} messages blocks or strings
 *  of info to be added
 * @return {MakeMKVInfo} full info returned by MakeMKV
 */
function processInfo(
    info = {
      drives: [],
      messages: [],
      titleCount: 0,
      titles: [],
    },
    ...messages
) {
  for (const m of messages.flatMap((m) =>
        typeof m === 'string' ?
            m
                .split(/[\r\n]+/)
                .filter((s) => s)
                .map(parseMsgLine) :
            m,
  )) {
    let title;
    let stream;
    switch (m.type) {
      case 'TCOUNT':
        info.titleCount = m.value;
        break;
      case 'MSG':
        info.messages.push(m);
        break;
      case 'DRV':
        if (m.visible != 256) info.drives.push(m);
        break;
      case 'CINFO':
        switch (m.code) {
          case 1:
            info.technology = m.value;
            break;
          case 2:
          case 30:
            info.name = m.value;
            break;
          case 31:
            info.rawDiskInfo = m.value;
            break;
          case 32:
            info.rawName = m.value;
            break;
          case 28:
            info.lang = m.value;
            break;
          case 29:
            info.language = m.value;
            break;
        }
        break;
      case 'TINFO':
        title = info.titles[m.id] || {id: m.id, streams: []};
        switch (m.code) {
          case 2:
            title.disc = m.value;
            break;
          case 8:
            title.chapters = m.value;
            break;
          case 9:
            title.duration = m.value;
            title.seconds = durationToSecs(m.value);
            break;
          case 10:
            title.humanSize = m.value;
            break;
          case 11:
            title.size = m.value;
            break;
          case 27:
            title.file = m.value;
            break;
          case 28:
            title.lang = m.value;
            break;
          case 29:
            title.language = m.value;
            break;
          case 30:
            title.name = m.value;
            break;
          case 31:
            title.raw = m.value;
            break;
        }
        info.titles[m.id] = title;
        break;
      case 'SINFO':
        title = info.titles[m.tid] || {id: m.tid, streams: []};
        stream = title.streams[m.id] || {id: m.id};
        switch (m.code) {
          case 1: // Audio, Video, Subtitles
            stream.type = m.value;
            break;
          case 5:
            stream.codecCode = m.value;
            break;
          case 6:
            stream.codec = m.value;
            break;
          case 7:
            stream.codecName = m.value;
            break;
          case 13:
            stream.bytes = m.value;
            break;
          case 17:
            stream.khz = m.value;
            break;
          case 19:
            stream.size = m.value;
            break;
          case 20:
            stream.ratio = m.value;
            break;
          case 21:
            stream.fps = m.value;
            break;
          case 28:
            stream.lang = m.value;
            break;
          case 29:
            stream.language = m.value;
            break;
          case 30:
            stream.name = m.value;
            break;
          case 31:
            stream.raw = m.value;
            break;
          case 39:
            stream.default = m.value === 'Default';
            break;
          case 40:
            stream.surround = m.value;
            break;
        }
        title.streams[m.id] = stream;
        info.titles[m.tid] = title;
        break;
    }
  }
  return info;
}

/**
 * Issues a makemkv command with progress bar
 * @param  {...String} options options to add to makemkv command
 * @return {Promise} promise fullfilled when command finishes
 */
function progressedCommand(...options) {
  const command = `makemkvcon -r --progress=-stderr ${options.join(' ')}`;
  console.debug(`issuing command: ${command}`);
  const multibar = new cliProgress.MultiBar(
      {
        format:
                // eslint-disable-next-line max-len
                '[{bar}] {percentage}% | {duration_formatted} | ETA: {eta_formatted} | {text}',
        clearOnComplete: true,
        emptyOnZero: true,
        hideCursor: true,
      },
      cliProgress.Presets.shades_grey,
  );
  let total = 65536;
  const currentBar = multibar.create(total, 0, {text: 'Current'});
  const totalBar = multibar.create(total, 0, {text: 'Total'});
  return new Promise((resolve, reject) => {
    let result;
    const child = exec(command, {maxBuffer: 8 * 1024 * 1024});
    child.on('error', reject);
    child.on('exit', (code, signal) =>
            code || signal ?
                reject(
                    new Error(
                        `Process finished with ${
                              code ? `retcode ${code}` : `signal ${signal}`
                        }`,
                    ),
                ) :
                resolve(result),
    );
    readline.createInterface({input: child.stdout}).on('line', (data) => {
      // console.debug('stdout', data)
      const msg = parseMsgLine(data);
      //   if (msg.type === 'MSG') console.debug(msg.message);
      result = processInfo(result, msg);
      // console.debug('stdout', result)
    });
    readline.createInterface({input: child.stderr}).on('line', (data) => {
      // console.debug('stderr', data)
      const msg = parseMsgLine(data);
      // console.debug('stderr', msg)
      switch (msg.type) {
        case 'PRGC':
          currentBar.update({text: msg.text});
          break;
        case 'PRGT':
          totalBar.update({text: msg.text});
          break;
        case 'PRGV':
          if (msg.max !== total) {
            totalBar.setTotal((total = msg.max));
            currentBar.setTotal(total);
          }
          currentBar.update(msg.current);
          totalBar.update(msg.total);
          break;
      }
    });
  }).finally(() => multibar.stop());
}

let drivesCache;

/**
 * Lists the drives available for reading discs.
 * @param {Boolean} scan true if discs should be scanned.
 *  Can interfere with other drives processes
 * @param {Boolean} cache true if discs shouldn't be scanned if cache available
 * @return {MaleMKVInfo} information retrieved
 */
async function listDrives(scan = false, cache = false) {
  if (cache && drivesCache) return drivesCache;
  const {stdout} = await execPromise(
      `makemkvcon -r --cache=1 ${scan ? '' : '--noscan'} info disc:9999`,
  );
  return (drivesCache = processInfo(undefined, stdout));
}

/**
 * Extracts the content of a disc to mkv files
 * @param {String} dir path for output directory
 * @param {Number} disc ID of the disc unit to use
 * @param {Number|String} title 'all' or numeric id of the title to rip
 * @param {Number} seconds minimum seconds for file
 * @return {Promise} the promise with the command execution
 */
async function rip(dir, disc = 0, title = 'all', seconds = 0) {
  await mkdir(dir);
  // Sobreescribe si hace falta
  return progressedCommand(
      '--noscan',
      '--directio=true',
      `--minlength=${seconds}`,
      '--decrypt',
      'mkv',
      `disc:${disc}`,
      title,
      `'${dir}'`,
  );
}

/**
 * Scans a disc repeiting the test if specified and necessary
 * @param {Boolean} wait true if scan should be repeated
 *  until one disc is detected
 * @param {Number} id number of disc to scan
 * @param {Number} seconds minimum seconds for file
 */
async function scanDisk(wait = true, id = 0, seconds = 0) {
  let result;
  do {
    if (result) await new Promise((resolve) => setTimeout(resolve, 3000));
    result = await progressedCommand(
        '--cache=1',
        '--noscan',
        `--minlength=${seconds}`,
        'info',
        `disc:${id}`,
    );
  } while (wait && !result.titleCount);
  return result;
}

/**
 * Creates a dir skipping if already exists
 * @param {String} dir path for dir to create
 * @param {Options} options fs.mkdir options
 */
async function mkdir(dir, options = {recursive: true}) {
  try {
    if (!(await await fsp.stat(dir)).isDirectory()) {
      await fsp.mkdir(dir, options);
    }
  } catch {
    await fsp.mkdir(dir, options);
  }
}

const extrasDir = 'extras';
const padNum = (num) => String(num).padStart(2, 0);

/**
 * Formats season dir name
 * @param {Serie} serie info of the series being processed
 * @return {String} season dir name
 */
function seasonName(serie) {
  return `Season ${padNum(serie.jellyfin.season)}`;
}

/**
 * Formats the new name for a movie or series file
 * @param {String} dir base path for movie or serie
 * @param {Info} info series or movie info
 * @return {String} new path for file
 */
function titleName(dir, info = {extras: 1}) {
  // Extras forlder for both movies and series
  // https://jellyfin.org/docs/general/server/media/movies.html#extras-folders
  if (info.extras) {
    return path.join(dir, extrasDir, `extras ${padNum(info.extras)}.mkv`);
  }
  if (info.jellyfin.season) {
    // Series separed by folder namig episodes if possible
    // https://jellyfin.org/docs/general/server/media/shows.html
    const episode = tmbd.getSeason(info, true);
    return path.join(
        dir,
        seasonName(info),
        `${(episode || info).jellyfin.safe} S${padNum(
            info.jellyfin.season,
        )}E${padNum(info.jellyfin.episode)}.mkv`,
    );
  } else {
    // Movie optionally versioned
    // https://jellyfin.org/docs/general/server/media/movies.html#multiple-versions-of-a-movie
    return path.join(
        dir,
        `${info.jellyfin.safe} - ${
                info.jellyfin.episode > 1 ?
                    `Version ${info.jellyfin.episode}` :
                    'Movie'
        }.mkv`,
    );
  }
}

/**
 * Renames a file
 * @param {String} source source path
 * @param {String} dest destination path
 * @return {Promise} promise resolved when finished
 */
function rename(source, dest) {
  console.log(
      chalk.bold.gray('Renaming'),
      chalk.magenta(source),
      chalk.bold.gray('->'),
      chalk.yellow(dest),
  );
  return fsp.rename(source, dest).catch(log.logError);
}

/**
 * Renames files according to its length and it can be movies, episodes
 * or extras
 * @param {String} dir path for base dir
 * @param {MakeMKVInfo} mkv info obtained fron makemkv scan
 * @param {Info} mdb info of the movie or series
 * @param {Number} extras offset of extra files
 */
async function renameFiles(dir, mkv, mdb, extras = {extras: 1}) {
  const maxTime =
        mdb?.jellyfin?.min ||
        Math.max(...mkv.titles.map((t) => t.seconds)) * 0.4;
  dir = path.join(dir, mdb.jellyfin.full);
  if (mdb.jellyfin.episode === undefined) mdb.jellyfin.episode = 1;
  for (const title of mkv.titles) {
    try {
      if (
        title.seconds > maxTime &&
                (!mdb?.jellyfin?.max || mdb.jellyfin.max > title.seconds)
      ) {
        if (mdb?.jellyfin?.season) {
          await mkdir(path.join(dir, seasonName(mdb)));
        }
        rename(path.join(dir, title.file), titleName(dir, mdb));
        mdb.jellyfin.episode++;
        if (
          mdb?.jellyfin?.season &&
                    tmbd.getSeason(mdb).episode_count < mdb.jellyfin.episode
        ) {
          mdb.jellyfin.episode = 1;
          mdb.jellyfin.season++;
        }
      } else {
        await mkdir(path.join(dir, extrasDir));
        rename(
            path.join(dir, title.file),
            titleName(dir, {extras: extras.extras++}),
        );
      }
    } catch (err) {
      log.logError(err);
    }
  }
}

module.exports = {
  durationToSecs,
  scanDisk,
  rip,
  listDrives,
  parseMsgLine,
  processInfo,
  renameFiles,
  mkdir,
};
